# Flask Example

> A simple book-store webapp using Flask and SQLite

This webapp is built around Flask and SQLite to provide a simple book-store,
allowing to add and search for books in a database.

## Usage

- `pipenv install -d`
- `pipenv shell`
- `cp .example.env .env`
- `flask run`
- Open [http://localhost:5000](http://localhost:5000)

## Documentation

- [Flask](https://flask.palletsprojects.com)
- [Jinja2](https://jinja.palletsprojects.com)
- [SQLite3](https://docs.python.org/3/library/sqlite3.html)
