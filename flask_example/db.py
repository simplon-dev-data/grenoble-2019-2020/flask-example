import sqlite3

from flask import g


def create_connection() -> sqlite3.Connection:
    db = sqlite3.connect("books.db")
    db.row_factory = sqlite3.Row
    return db


def close_connection(conn: sqlite3.Connection) -> None:
    if conn is not None:
        conn.close()


def create_tables(conn: sqlite3.Connection) -> None:
    q = """
        CREATE TABLE IF NOT EXISTS books (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT UNIQUE NOT NULL,
            author TEXT NOT NULL,
            price INTEGER NOT NULL,
            currency TEXT NOT NULL
        )
    """
    with conn:
        conn.execute(q)


def drop_tables(conn: sqlite3.Connection) -> None:
    q = """ DROP TABLE books """
    with conn:
        conn.execute(q)


def count_all_books(conn: sqlite3.Connection):
    q = """ SELECT COUNT(*) as num_books FROM books"""
    num_books = 0
    with conn:
        r = conn.execute(q).fetchone()
        num_books = r["num_books"]
    return num_books


def get_all_books(conn: sqlite3.Connection, title_search="", page=1, limit=3, order_by="price", sort="descending") -> list:
    sort_values = {
        "descending": "DESC",
        "ascending": "ASC"
    }

    q = f"""
        SELECT * FROM books
        WHERE title LIKE :title_search
        ORDER BY {order_by} {sort_values.get(sort, sort_values["descending"])}
        LIMIT :limit
        OFFSET :offset
    """

    p = {
        "title_search": "%" + title_search + "%",
        "limit": limit,
        "offset": (page-1) * limit,
    }

    books = []
    with conn:
        books = conn.execute(q, p).fetchall()
    return books


def add_book(conn: sqlite3.Connection, book) -> None:
    q = """
        INSERT INTO books (title, author, price, currency)
        VALUES (:title, :author, :price, :currency)
    """
    with conn:
        conn.execute(q, book)


def get_db() -> sqlite3.Connection:
    if "db" not in g:
        g.db = create_connection()
    return g.db


def close_db(e=None) -> None:
    db = g.pop("db", None)
    close_connection(db)


def init_app(app) -> None:
    conn = create_connection()
    create_tables(conn)
    app.teardown_appcontext(close_db)
