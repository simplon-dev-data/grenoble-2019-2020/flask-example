from flask import Flask, redirect, render_template, request, url_for
from . import db


app = Flask(__name__)
db.init_app(app)


@app.route("/", methods=["GET"])
def get_homepage():
    title_search = request.args.get("title_search", "")
    page = int(request.args.get("page", 1))
    limit = int(request.args.get("limit", 5))
    order_by = request.args.get("order_by", "price")
    sort = request.args.get("sort", "descending")

    conn = db.get_db()
    books = db.get_all_books(
        conn,
        title_search=title_search,
        page=page,
        limit=limit,
        order_by=order_by,
        sort=sort
    )

    num_books = db.count_all_books(conn)

    prev_url = None
    if (page > 1):
        next_url_args = dict(request.args)
        next_url_args["page"] = page - 1
        prev_url = url_for("get_homepage", **next_url_args)

    next_url = None
    if (page * limit) < num_books and len(books) == limit:
        next_url_args = dict(request.args)
        next_url_args["page"] = page + 1
        next_url = url_for("get_homepage", **next_url_args)

    return render_template(
        "homepage.html",
        books=books,
        num_books=num_books,
        title_search=title_search,
        limit=limit,
        order_by=order_by,
        sort=sort,
        prev_url=prev_url,
        next_url=next_url)


@app.route("/new", methods=["GET"])
def get_new():
    return render_template("new.html")


@app.route("/new", methods=["POST"])
def add_new():
    conn = db.get_db()
    db.add_book(conn, request.form)
    return redirect(url_for("get_homepage"))
